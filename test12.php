<?php

interface iDB {
    /**
     * @param string $host Adres DB
     * @param string $login Login użytkownika
     * @param string $password Hasło użytkownika
     * @param string $dbName Nazwa bazy danych
     */
    public function __construct($host, $login, $password, $dbName);

    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * @param string $query Zapytanie do wykonania
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query);

    /**
     * Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * @return int
     */
    public function getAffectedRows();

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getRow();

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getAllRows();
}

class DB implements iDB
{
    private $link;
    private $queryResult;
    
    /**
     * @param string $host Adres DB
     * @param string $login Login użytkownika
     * @param string $password Hasło użytkownika
     * @param string $dbName Nazwa bazy danych
     */
    public function __construct($host, $login, $password, $dbName)
    {
        $this->link = mysqli_connect($host, $login, $password, $dbName);
        if (!$this->link) {
            throw new Exception ('Nie udalo sie polaczyc z baza danych: ' . $dbName);
        }
    }

    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * 
     * @param string $query Zapytanie do wykonania
     * 
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query)
    {
        $this->queryResult = mysqli_query($this->link, $query);
        
        return (bool)$this->queryResult;
    }

    /**
     * Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * 
     * @return int
     */
    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->link);
    }

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * 
     * @return mixed
     */
    public function getRow()
    {
        if (!$this->queryResult) {
            throw new Exception('Zapytanie nie zostalo wykonane');
        }
        
        return mysqli_fetch_assoc($this->queryResult);
    }

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * 
     * @return mixed
     */
    public function getAllRows()
    {
        $results = array();
        
        while (($row = $this->getRow())) {
            $results[] = $row;
        }
        
        return $results;
    }
}


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    
</head>
<body>
    <form action="" method="post">
    
    Name: <input type="text" name="name"><br />
    Surname: <input type="text" name="surname"><br />
    Gender: <br />
    <select multiple name="gender">
        <option value="male">male</option>
        <option value="female">female</option>
        <option value="other">other</option>
    </select><br />
    Date of birth: <input type="datetime-local" name="date"><br />
    Street: <input type="text" name="street"><br />
    City: <input type="text" name="city"><br />
    Postcode <input type="text" name="postcode"><br />
    Notes <textarea name="notes" rows="10" cols="30"></textarea>
    <input type="submit" value="Wyślij mnie!" />
</form>
</body>
</html>
<?php 
$DB = new DB('localhost','root','','client');
$DB->query(SELECT * FROM `clients` WHERE `id` = '975007');
	
 ?>