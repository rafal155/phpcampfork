<?php

interface iDB {
    /**
     * @param string $host Adres DB
     * @param string $login Login użytkownika
     * @param string $password Hasło użytkownika
     * @param string $dbName Nazwa bazy danych
     */
    public function __construct($host, $login, $password, $dbName);

    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * @param string $query Zapytanie do wykonania
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query);

    /**
     * Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * @return int
     */
    public function getAffectedRows();

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getRow();

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getAllRows();
}

class DB implements iDB
{
    private $link;
    private $queryResult;
    
    /**
     * @param string $host Adres DB
     * @param string $login Login użytkownika
     * @param string $password Hasło użytkownika
     * @param string $dbName Nazwa bazy danych
     */
    public function __construct($host, $login, $password, $dbName)
    {
        $this->link = mysqli_connect($host, $login, $password, $dbName);
        if (!$this->link) {
            throw new Exception ('Nie udalo sie polaczyc z baza danych: ' . $dbName);
        }
    }

    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * 
     * @param string $query Zapytanie do wykonania
     * 
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query)
    {
        $this->queryResult = mysqli_query($this->link, $query);
        
        return (bool)$this->queryResult;
    }

    /**
     * Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * 
     * @return int
     */
    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->link);
    }

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * 
     * @return mixed
     */
    public function getRow()
    {
        if (!$this->queryResult) {
            throw new Exception('Zapytanie nie zostalo wykonane');
        }
        
        return mysqli_fetch_assoc($this->queryResult);
    }

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * 
     * @return mixed
     */
    public function getAllRows()
    {
        $results = array();
        
        while (($row = $this->getRow())) {
            $results[] = $row;
        }
        
        return $results;
    }
}


$db = new DB('localhost','root','','client');
//$DB->query('SELECT * FROM `products`');

//$DB->query('SELECT * FROM `products` LIMIT 2');

/*if(isset($_GET['action']))
{
    $products=$_GET['products'];
    $action=$_GET['action'];
    $name=$_GET['name'];
    $action=$_GET['price'];
}

if($action=checkProduct)
{
$DB->query("SELECT * FROM `products` WHERE nazwa='$name'");
var_dump($DB->getAllRows());
}

if($action==addProduct)
{
   var_dump($DB->getAllRows()); 
}

if($action==removeProduct)
{
    var_dump($DB->getAllRows());
}



print $DB->getAffectedRows() . '<br>';
var_dump($DB->getRow());
var_dump($DB->getAllRows());
*/
if (isset($_GET['action']))
{
    $product = $_GET['product'];
    $action = $_GET['action'];
    $name = $_GET['name'];
    $price = $_GET['price'];
    
    if ( $action == "checkProduct" )
    {
        $db->query("SELECT * FROM `products`  WHERE `id` = '$product'");
        $products = $db->getAllRows();
        var_dump($products);
    }
    elseif ( $action == "addProduct" )
    {
        $db->query("INSERT INTO `products` SET `nazwa` = '$name', `price` = '$price'");       
    }    
    elseif ( $action == "removeProduct" )
    {
        $db->query("DELETE FROM `products` WHERE `id` = '$product'");        
    }       
}

?>


