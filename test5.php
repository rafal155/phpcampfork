<?php 

class Rezerwacja
{
	public $pokoj;
	public $przyjazd;
	public $odjazd;

	public function __construct($pokoj,$przyjazd,$odjazd)
	{
		$this->pokoj = $pokoj;
		$this->przyjazd = $przyjazd;
		$this->odjazd = $odjazd;
	}
	
	public function __toString()
	{
		return 'termin rezerwacji pokoju:' .  $this->pokoj .'od' . $this->przyjazd . 'do' . $this->odjazd;
	}
}
$wyjazd = new Rezerwacja('123','2017-06-12','2017-06-18');
echo $wyjazd;

 ?>